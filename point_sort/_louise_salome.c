// Salomé et louise 
// partie regner pour déterminer la plus petit distance dans un nuage de points.


#include <stdint.h>
#include <stdio.h>

typedef struct {
    int x, y;
}point;

typedef struct { 
    point p1, p2;
    int d;

} triplet ;

typedef struct 
{
    int taille1;
    point* tab1;
    int taille2;
    point* tab2;

}deux_tableau;

int ecart (point* p1, point* p2);
triplet regner (point* tab);


int ecart (point* p1, point* p2)
{
    /* mesure l'ecart entre deux points */
    int ecart_x = (p1-> x)-(p2 -> x);
    int ecart_y = (p1-> y)-(p2 -> y);
    return ecart_x * ecart_x + ecart_y * ecart_y ;
}

triplet regner (point* tab){
    int e1 = ecart(&tab[0], &tab[1]);
    int e2 = ecart(&tab[1], &tab[2]);
    int e3 = ecart(&tab[0], &tab[2]);
    if  (e1 < e3) {
        if (e1 <e2){
            return (triplet){tab[0], tab[1], e1};
        }
        else {
            return (triplet){tab[1], tab[2], e2};
        }
    }
    else{
        if (e3 <e2){
            return (triplet){tab[0], tab[2], e3};
        if  (e1 < e3) {
        }
        else {
            return (triplet){tab[1], tab[2], e2};
        }
    }
}

}


deux_tableau division(point *tab, int taille){
    // Fonction non testée
    int taille1 = taille/2 +taille%2 ;
    int taille2 = taille/2;
    point *tab1, *tab2;
    tab1[0] = tab[0];
    tab2[0] = tab[taille/2+ taille%2 ];
    return (deux_tableau) { taille1, tab1,taille2,  tab2 };
} 

int main(){
    point t[3] = {{2, 3},{2,4},{5,8}};
    triplet a = regner(t);
    printf("Les points de coordonnes \n\t x = %d et y = %d \n x = %d et y = %d\n sont les plus proche et ont pour distance %d.\n", a.p1.x, a.p1.y, a.p2.x, a.p2.y, a.d);
    deux_tableau b;
    b = division(&t, 3);
    printf( "")
    return 0;
}