#include "nuage.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>


point* NuageGenereAleatoire(int n)
{
    point* n_point=malloc(n*sizeof(point));
    assert(n_point!=NULL);
    int x,y;
    for(int i=0;i<n;i++){
        x=rand()%200-100;
        y=rand()%200-100;
        n_point[i].x=x;
        n_point[i].y=y;
    }
    return n_point;
}

void NuageAffiche(point* n_point,int n){
    printf("[");
    for(int i=0;i<n;i++){
        printf("(");
        printf("%d",n_point[i].x);
        printf(",");
        printf("%d",n_point[i].y);
        printf(")\n");
    }
}
