#include <assert.h>
#include <stdlib.h>

#include "fusion.h"


void FusionSort_divide(point *source, int source_size, point **tab1, int *tab1_size, point **tab2, int *tab2_size);
point *FusionSort_fusion(const point *tab1, int tab1_size, const point *tab2, int tab2_size);


point *FusionSort(point *tab, int size)
{
    assert(size >= 0 && "FusionSort : size should be positive !");

    if (size == 0) {
        return NULL;
    } else if (size == 1) {
        point *ret = malloc(sizeof(point) * 1);
        ret[0] = tab[0];
        return ret;
    }

    point *tab1, *tab2;
    int tab1_size, tab2_size;

    FusionSort_divide(tab, size, &tab1, &tab1_size, &tab2, &tab2_size);

    point *tab1_sorted = FusionSort(tab1, tab1_size);
    point *tab2_sorted = FusionSort(tab2, tab2_size);

    point *ret = FusionSort_fusion(tab1_sorted, tab1_size, tab2_sorted, tab2_size);
    free(tab1_sorted);
    free(tab2_sorted);

    return ret;
}


void FusionSort_divide(point *source, int source_size, point **tab1, int *tab1_size, point **tab2, int *tab2_size)
{
    *tab1 = source;
    *tab1_size = source_size / 2;

    *tab2 = &source[*tab1_size];
    *tab2_size = source_size - *tab1_size;
}


point *FusionSort_fusion(const point *tab1, int tab1_size, const point *tab2, int tab2_size)
{
    point *ret = malloc(sizeof(point) * (tab1_size + tab2_size));

    int i1 = 0;
    int i2 = 0;

    while (i1 < tab1_size || i2 < tab2_size) {
        if (i1 >= tab1_size) {
            ret[i1 + i2] = tab2[i2];
            i2++;
        } else if (i2 >= tab2_size) {
            ret[i1 + i2] = tab1[i1];
            i1++;
        } else if (tab1[i1].x <= tab2[i2].x) {
            ret[i1 + i2] = tab1[i1];
            i1++;
        } else {
            ret[i1 + i2] = tab2[i2];
            i2++;
        }
    }

    return ret;
}
