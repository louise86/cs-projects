#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <assert.h>

typedef struct{
    int x;
    int y;
}point;

point* Genere_nuage(int n,int xmax,int ymax){
    point* n_point=malloc(n*sizeof(point));
    assert(n_point!=NULL);
    int x,y;
    for(int i=0;i<n;i++){
        x=rand()%(2*xmax)-xmax;
        y=rand()%(2*ymax)-ymax;
        n_point[i].x=x;
        n_point[i].y=y;
    }
    return n_point;
}

void AfficheNuage(point* n_point,int n){
    printf("[");
    for(int i=0;i<n;i++){
        printf("(");
        printf("%d",n_point[i].x);
        printf(",");
        printf("%d",n_point[i].y);
        printf(")\n");
    }
}

void AfficheNuageGraphe(point* n_point,int n,int xmax,int ymax){
    char** tab_tab_c=malloc(((2*ymax)+1)*sizeof(char*));
    assert(tab_tab_c!=NULL);
    for(int i=0;i<(2*ymax);i++){
        tab_tab_c[i]=malloc(((2*xmax)+2)*sizeof(char));
        assert(tab_tab_c[i]!=NULL);
    }
    for(int l=0;l<(2*ymax);l++){
        for(int k=0;k<(2*xmax+1);k++){
            tab_tab_c[l][k]=' ';
        }
    }
    for(int j=0;j<(2*xmax+1);j++){
        tab_tab_c[ymax][j]='-';
    }
    for(int m=0;m<(2*ymax);m++){
        tab_tab_c[m][xmax]='|';
        tab_tab_c[m][2*xmax+1]='\0';
    }
    tab_tab_c[ymax][xmax]='+';
    for(int t=0;t<n;t++){
        tab_tab_c[(n_point[t].y)+ymax][(n_point[t].x)+xmax]='.';
    }

    for(int o;o<(2*ymax);o++){
        printf(tab_tab_c[o]);
        printf("\n");
    }
}

int main(){
   srand(time(NULL)); 
   point* NuagePoint=Genere_nuage(150,110,30);
   //AfficheNuage(NuagePoint,100);
    AfficheNuageGraphe(NuagePoint,150,110,30);
   return 0;
}

