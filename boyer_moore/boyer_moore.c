#include <stdlib.h>
#include <stdio.h>
#include <string.h>


int ** table_decalage(char* motif, int taille);
void affiche_table_decalage(int ** decalage,int taille);
int boyer_moore(char * motif, char* texte);

int ** table_decalage(char* motif, int taille){
    int * ascii = malloc(sizeof(int)*256);
        if (ascii == NULL){
            return NULL;
        }
    int ** decalage= malloc( sizeof(int)*256*taille);
    if (decalage == NULL){
            return NULL;     
    }

    for (int i = 0; i<256; i ++ ){
        ascii[i] = -1;}
    decalage[0] = ascii;
    for (int i =1; i<taille; i++){
        int * ascii = malloc(sizeof(int)*256);
        if (ascii == NULL){
            return NULL;
        }
        for (int j = 0 ; j<256; j++){
            ascii[j] = decalage[i-1][j];
        }
        ascii[(int) motif[i]] = i;
        decalage[i] = ascii;
    }
    return decalage;
}

void affiche_table_decalage(int ** decalage, int taille)
{   
    printf("Affichage de la table de decalage :\n[");
    for (int i = 0  ; i<taille ; i ++){
        for (int j = 0 ; j< 256 ; j ++){
            printf("\t%d", decalage[i][j]);
        }
        printf("]\n[");
    }
}

int boyer_moore(char *motif, char *texte)
{
    int taille_m = strlen(motif);
    int taille_t = strlen(texte);
    int indice_texte = 0;
    int indice_motif = taille_m -1;
    int **decalage = table_decalage(motif, taille_m);
    int nb_occurrences = 0;
    // tableau des occurrences
    while (indice_texte < taille_t - taille_m+1) {

        for (int i = 0 ;i< taille_m; i++){
            if (motif[indice_motif-i] != texte[indice_motif+indice_texte-i]){
                if (decalage[indice_motif-i][  (int) texte[indice_motif+indice_texte-i] ] == -1 ){
                    indice_texte = indice_motif+indice_texte-i +1 ;
                }
                else{
                    indice_texte = indice_texte + decalage[ indice_motif-i][ (int) texte[indice_motif+indice_texte-i] ];
                }
                break;
            }
            if (i == taille_m -1){
                nb_occurrences += 1 ;;
            indice_texte ++;}
        }
    }
    return nb_occurrences;
}

int main(){
    char motif[4] = "jjik";
    int taille = 4;
    int ** decalage = table_decalage(motif, taille);
    affiche_table_decalage(decalage, taille);
    printf("Soit, %d occurrences de an dans banane.", boyer_moore("an", "banane"));
    return 0;
}